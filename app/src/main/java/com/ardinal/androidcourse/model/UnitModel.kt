package com.ardinal.androidcourse.model

import com.google.gson.annotations.SerializedName

data class UnitResponse (
    @SerializedName("body")
    val body: List<UnitItem>?
)

data class UnitItem (
    @SerializedName("title")
    val title: String?,

    @SerializedName("subtitle")
    val subtitle: List<SubtitleItem>?
)

data class SubtitleItem (
    @SerializedName("subtitle_id")
    val subtitle_id: Int?,

    @SerializedName("subtitle_name")
    val subtitle_name: String?,

    @SerializedName("content")
    val content: List<ContentItem>?
)

data class ContentItem (
    @SerializedName("section_id")
    val section_id: Int?,

    @SerializedName("content_id")
    val content_id: Int?,

    @SerializedName("number")
    val number: String?,

    @SerializedName("title")
    val title: String?
)