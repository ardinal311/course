package com.ardinal.androidcourse.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ardinal.androidcourse.R
import com.ardinal.androidcourse.model.ContentItem
import kotlinx.android.synthetic.main.item_unit_content.view.*

class UnitContentAdapter(var data: List<ContentItem> = listOf(), var clickListener: (ContentItem) -> Unit)
    : RecyclerView.Adapter<UnitContentHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): UnitContentHolder {
        return UnitContentHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_unit_content, viewGroup, false)
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: UnitContentHolder, position: Int) {
        holder.bindData(data[position], clickListener)
    }
}

class UnitContentHolder(view: View): RecyclerView.ViewHolder(view) {

    fun bindData(data: ContentItem, clickListener: (ContentItem) -> Unit) {
        itemView.unit_content_name.text = data.title
        itemView.unit_content_number.text = data.number

        itemView.setOnClickListener {
            clickListener(data)
        }
    }
}