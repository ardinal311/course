package com.ardinal.androidcourse.module.unit_one.hello

import android.content.Context
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.ardinal.androidcourse.MainActivity
import com.ardinal.androidcourse.R

class HelloRelative : MainActivity() {

    var counter: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_hello_toast_constraint)
        supportActionBar?.setDisplayShowTitleEnabled(true)

        val toastButton = findViewById<Button>(R.id.hello_toast_button)
        val countButton = findViewById<Button>(R.id.hello_count_button)
        val countText = findViewById<TextView>(R.id.hello_show_count)

        toastButton.setOnClickListener {
            showToast(this, "Hello Toast")
        }

        countButton.setOnClickListener {
            countUp(countText)
        }
    }

    private fun showToast(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    private fun countUp(text: TextView?) {
        counter++
        if (text != null) {
            text.text = counter.toString()
        }
    }
}