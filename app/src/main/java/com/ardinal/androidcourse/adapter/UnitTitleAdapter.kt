package com.ardinal.androidcourse.adapter

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ardinal.androidcourse.R
import com.ardinal.androidcourse.model.SubtitleItem
import com.ardinal.androidcourse.model.UnitItem
import kotlinx.android.synthetic.main.item_unit_title.view.*

class UnitTitleAdapter(var data: List<UnitItem> = listOf()): RecyclerView.Adapter<UnitTitleHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): UnitTitleHolder {
        return UnitTitleHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_unit_title, viewGroup, false)
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: UnitTitleHolder, position: Int) {
        holder.bindData(data[position])
    }
}

class UnitTitleHolder(view: View): RecyclerView.ViewHolder(view) {

    private var dataAdapter = UnitSubtitleAdapter()

    fun bindData(data: UnitItem) {
        itemView.unit_title_name.text = data.title
        itemView.unit_title_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = dataAdapter
        }

        if (data.subtitle != null) {
            dataAdapter.data = data.subtitle
            dataAdapter.notifyDataSetChanged()
        }
    }

    private fun subtitleClicked(data: SubtitleItem?) {

    }
}