package com.ardinal.androidcourse.module.unit_one.hello

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import com.ardinal.androidcourse.MainActivity
import com.ardinal.androidcourse.R
import kotlinx.android.synthetic.main.component_toolbar.*
import kotlinx.android.synthetic.main.layout_hello_world.*

class HelloWorld: MainActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_framing)

        setupView()
    }

    private fun setupView() {
        setupToolbar()
        setupContainer()
        setupLink()
    }

    private fun setupToolbar() {
        supportActionBar?.hide()
        toolbar_title.text = getString(R.string.hello_world)
        toolbar_back_button.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setupContainer() {
        val container = findViewById<FrameLayout>(R.id.framing_frame_layout)
        val view = createCustomView(this, R.layout.layout_hello_world, container)
        container.addView(view)
    }

    private fun setupLink() {
        challenge_button.let {
            it.isClickable = true
            it.setOnClickListener(this)
        }

    }

    override fun onClick(view: View?) {
        when(view?.id) {
            challenge_button.id -> {
                openChallenge()
            }
        }
    }

    private fun openChallenge() {
        val intent = Intent(this, HelloWorldChallenge::class.java)
        startActivity(intent)
    }
}