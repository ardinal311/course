package com.ardinal.androidcourse.module.unit_one

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.ardinal.androidcourse.R

class FirstUnitActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.scrolling_text)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)
    }
}