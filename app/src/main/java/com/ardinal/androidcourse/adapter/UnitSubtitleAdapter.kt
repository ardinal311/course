package com.ardinal.androidcourse.adapter

import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ardinal.androidcourse.MainActivity
import com.ardinal.androidcourse.R
import com.ardinal.androidcourse.model.ContentItem
import com.ardinal.androidcourse.model.SubtitleItem
import kotlinx.android.synthetic.main.item_unit_subtitle.view.*

class UnitSubtitleAdapter(var data: List<SubtitleItem> = listOf())
    : RecyclerView.Adapter<UnitSubtitleHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): UnitSubtitleHolder {
        return UnitSubtitleHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_unit_subtitle, viewGroup, false)
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: UnitSubtitleHolder, position: Int) {
        holder.bindData(data[position])
    }
}

class UnitSubtitleHolder(view: View): RecyclerView.ViewHolder(view) {

    private var dataAdapter = UnitContentAdapter(clickListener = { data : ContentItem -> contentClicked(data) } )
    private var activity = itemView.context as MainActivity

    fun bindData(data: SubtitleItem) {
        itemView.unit_subtitle_name.text = data.subtitle_name

        itemView.unit_subtitle_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = dataAdapter
        }

        if(data.content != null) {
            dataAdapter.data = data.content
            dataAdapter.notifyDataSetChanged()
        }

        itemView.unit_subtitle_card.setOnClickListener {
            if(itemView.unit_subtitle_recycler_view.visibility == View.VISIBLE) {
                itemView.unit_subtitle_recycler_view.visibility = View.GONE
                itemView.unit_subtitle_icon.setImageDrawable(
                    ResourcesCompat.getDrawable(
                    it.resources, R.drawable.ic_chevron_right, null))
            }
            else {
                itemView.unit_subtitle_recycler_view.visibility = View.VISIBLE
                itemView.unit_subtitle_icon.setImageDrawable(ResourcesCompat.getDrawable(
                    it.resources, R.drawable.ic_chevron_down, null))
            }
        }
    }

    private fun contentClicked(data: ContentItem) {
        activity.lessonClicked(data)
    }
}