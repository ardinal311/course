package com.ardinal.androidcourse

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ardinal.androidcourse.adapter.UnitTitleAdapter
import com.ardinal.androidcourse.model.ContentItem
import com.ardinal.androidcourse.model.UnitItem
import com.ardinal.androidcourse.model.UnitResponse
import com.ardinal.androidcourse.module.unit_one.FirstUnitActivity
import com.ardinal.androidcourse.module.unit_one.hello.HelloToast
import com.ardinal.androidcourse.module.unit_one.hello.HelloWorld
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import java.io.InputStream

open class MainActivity : AppCompatActivity() {

    var dataAdapter = UnitTitleAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupView()
    }

    private fun setupView() {
        setupToolbar()
        fetchData()
        setupRecyclerView()
    }

    private fun setupToolbar() {
        supportActionBar?.hide()
    }

    private fun fetchData() {
        val stream: InputStream = this.assets.open("unit.json")

        var output = ""
        stream.bufferedReader().use {
            output = it.readText()
        }

        val gson = Gson()
        val result : UnitResponse = gson.fromJson(output, UnitResponse::class.java)
        showData(result.body)
    }

    private fun setupRecyclerView() {
        unit_main_recycler_view.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = dataAdapter
        }
    }

    private fun showData(data: List<UnitItem>?) {
        if(data != null) {
            dataAdapter.data = data
            dataAdapter.notifyDataSetChanged()
        }
    }

    fun lessonClicked(data: ContentItem?) {
        when(data?.section_id) {
            1 -> gotoSectionOne(data.content_id)
            2 -> gotoSectionTwo(data.content_id)
        }
    }

    fun gotoSectionOne(id: Int?) {
        when(id) {
            1 -> openLesson(HelloWorld())
            2 -> openLesson(HelloToast())
            3 -> openLesson(FirstUnitActivity())
            4 -> openLesson(FirstUnitActivity())
        }
    }

    fun gotoSectionTwo(id: Int?) {

    }

    fun openLesson(activity: AppCompatActivity) {
        val intent = Intent(this, activity::class.java)
        startActivity(intent)
    }

    fun createCustomView(context: Context, layout: Int, container: ViewGroup): View {
        val inflater = LayoutInflater.from(context)
        return inflater.inflate(layout, container, false)
    }
}