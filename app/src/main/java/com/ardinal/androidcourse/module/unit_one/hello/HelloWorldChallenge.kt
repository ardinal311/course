package com.ardinal.androidcourse.module.unit_one.hello

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.FrameLayout
import com.ardinal.androidcourse.MainActivity
import com.ardinal.androidcourse.R
import kotlinx.android.synthetic.main.component_toolbar.*

class HelloWorldChallenge : MainActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_framing)
        setupView()
    }

    private fun setupView() {
        setupToolbar()
        setupContainer()
    }

    private fun setupToolbar() {
        supportActionBar?.hide()
        toolbar_title.text = getString(R.string.hello_world_challenge)
        toolbar_back_button.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setupContainer() {
        val container = findViewById<FrameLayout>(R.id.framing_frame_layout)
        val view = createCustomView(this, R.layout.layout_hello_world_challenge, container)
        container.addView(view)
    }
}